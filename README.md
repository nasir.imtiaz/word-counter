# Akka Word Counter

A standalone **Akka** application based on **Java 8** to read a given file and display total number of word count.

### Features
The application features some of the aspects of Akka toolkit, namely:
  - *ActorSystem*
  - *Actor*
  - *Actor hierarchy (parent-child)*
  - *Message*

### How to run:
  - clone this project into your local directory: 
  `git clone https://gitlab.com/nasir.imtiaz/word-counter.git`
  - Build the project: 
  `mvn clean install`
  - Run the `spring-bootstrap` project:
  `java -jar word-counter-0.0.1-SNAPSHOT-spring-boot.jar`