package com.m800.akka;

import static com.m800.akka.common.Constant.ACTOR_SYSTEM_NAME;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.m800.akka.service.WordCountService;
import com.m800.akka.service.WordCountServiceImpl;

import akka.actor.ActorSystem;

/**
 * This is the main class to trigger Word Counting.
 *
 */
public class Application {
	static final Logger logger  = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args) {
		Application application;
		String filePath;
		
		application = new Application();
		
		filePath = application.readFilePath();
		
		application.countWordsInFile(filePath);
	}

	/**
	 * Method to take file path input from user
	 * 
	 */
	public String readFilePath() {
		System.out.println("Enter complete file path (including filename): ");
		Scanner scanner = new Scanner(System.in);
		String filePath = scanner.nextLine();
		logger.info("Your provided file path is " + filePath);
		return filePath;
	}

	/**
	 * Method to initiate word count process
	 * 
	 */
	public void countWordsInFile(String filePath) {
		final ActorSystem system = ActorSystem.create(ACTOR_SYSTEM_NAME);
		
		// Initializing and starting ActorSystem
		
		logger.info("Creating ActorSystem '{}'", ACTOR_SYSTEM_NAME);
		
		// Initiating word count process
		
		WordCountService wordCounter = new WordCountServiceImpl();
		
		logger.debug("Calling wordCounter.countWordsInFile({}, {})", filePath, system.toString());
		wordCounter.countWordsInFile(filePath, system);
	}
}
