package com.m800.akka.service;

import akka.actor.ActorSystem;

public interface WordCountService {
	public void countWordsInFile(String filePath, ActorSystem system);
}
