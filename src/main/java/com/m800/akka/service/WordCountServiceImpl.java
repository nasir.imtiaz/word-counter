package com.m800.akka.service;

import static com.m800.akka.common.Constant.FILE_PARSER_ACTOR_NAME;
import static com.m800.akka.common.Constant.FILE_SCANNER_ACTOR_NAME;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.m800.akka.actor.FileParserActor;
import com.m800.akka.actor.FileScannerActor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

public class WordCountServiceImpl implements WordCountService {
	static final Logger logger = LoggerFactory.getLogger(WordCountServiceImpl.class);
	
	@Override
	public void countWordsInFile(String filePath, ActorSystem system) {
		// Creating a reference to file parser actor to be passed on to file scanner actor for file parsing
		
		logger.info("Creating actor '{}'", FILE_PARSER_ACTOR_NAME);
		final ActorRef fileParserActorRef = system.actorOf(FileParserActor.props(), FILE_PARSER_ACTOR_NAME);
		
		// Creating a reference to file scanner actor and passing file name and a reference to file parser actor 
		
		logger.info("Creating actor '{}'", FILE_SCANNER_ACTOR_NAME);
		final ActorRef fileScannerActorRef = system.actorOf(FileScannerActor.props(filePath, fileParserActorRef), FILE_SCANNER_ACTOR_NAME);

		// Sending 'Scan' message to file scanner actor
		
		logger.info("Sending '{}' message to actor '{}'", "ScanMessage", FILE_SCANNER_ACTOR_NAME);
		fileScannerActorRef.tell(new FileScannerActor.ScanMessage(), ActorRef.noSender());
	}
}
