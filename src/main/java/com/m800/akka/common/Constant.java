package com.m800.akka.common;

public class Constant {
	public static final String ACTOR_SYSTEM_NAME = "actorSystem";
	public static final String FILE_SCANNER_ACTOR_NAME = "fileScannerActor";
	public static final String FILE_PARSER_ACTOR_NAME = "fileParserActor";
	public static final String AGGREGATOR_ACTOR_NAME = "aggregatorActor";
}
