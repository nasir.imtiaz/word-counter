package com.m800.akka.common;

import java.io.File;

import akka.actor.AbstractActor.ActorContext;

public class Util {
	public static boolean isFileExist(String filePathString) {
		File f = new File(filePathString);
		return f.exists();
	}
	
	public static void terminateActorSystem(ActorContext actorContext) {
		actorContext.system().terminate();
	}
}
