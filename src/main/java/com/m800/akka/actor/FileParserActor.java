package com.m800.akka.actor;

import static com.m800.akka.common.Constant.ACTOR_SYSTEM_NAME;
import static com.m800.akka.common.Constant.AGGREGATOR_ACTOR_NAME;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.m800.akka.common.Util;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;;

/**
 * File Parser Actor for parsing lines in the given file and forwarding each
 * line in the file to it's child i.e. Aggregator Actor
 *
 */
public class FileParserActor extends AbstractActor {
	static final Logger logger = LoggerFactory.getLogger(FileParserActor.class);
	
	/**
	 * Initializing relevant actor properties
	 */
	public static Props props() {
		return Props.create(FileParserActor.class);
	}

	/**
	 * Definition of 'Parse' message for parsing a file
	 * 
	 */
	public static class ParseMessage {
		public final String filePath;

		public ParseMessage(String filePath) {
			this.filePath = filePath;
		}

		@Override
		public String toString() {
			return "ParseMessage [filePath=" + filePath + "]";
		}
	}

	/**
	 * Callback method triggered prior to receiving a message 
	 */
	@Override
	public void preStart() {
		logger.debug("Starting '{}'", getSelf().path().name());
	}

	/**
	 * Callback method triggered prior to restarting an actor
	 */
	@Override
	public void preRestart(Throwable reason, Optional<Object> message) {
		logger.error("Restarting due to {} when processing {}", reason.getMessage(), message.isPresent() ? message.get() : "");
	}
	
	/**
	 * Callback method triggered on receiving a message
	 */
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(ParseMessage.class, this::onParse) // Matching 'Parse' message
				.build();
	}

	/**
	 * Message handler to parse a file and send each line as a separate message to aggregator actor to count the number of words
	 */
	private void onParse(ParseMessage parseMessage) {
		logger.debug("Parsing file '{}'", parseMessage.filePath);

		// Creating a reference to child actor (i.e. 'aggregator')
		
		final ActorRef aggregatorActorRef = getContext().actorOf(AggregatorActor.props(), AGGREGATOR_ACTOR_NAME);

		// Asking parser actor to watch over its child actor (i.e. 'aggregator') in case of a failure
		
		getContext().watch(aggregatorActorRef);
		
		// Sending 'Start of file' message to aggregator actor in order to
		// inform the file reading process has initiated 

		logger.info("Sending '{}' message to actor '{}'", "StartOfFileMessage", AGGREGATOR_ACTOR_NAME);
		aggregatorActorRef.tell(new AggregatorActor.StartOfFileMessage(), getSelf());

		// Sending 'Line' message(s) to aggregator actor in order to
		// read and accumulate number of words in the given line 

		logger.info("Sending '{}' message(s) to actor '{}'", "LineMessage", AGGREGATOR_ACTOR_NAME);
		try (Stream<String> stream = Files.lines(Paths.get(parseMessage.filePath))) {
			stream.forEach(line -> aggregatorActorRef.tell(new AggregatorActor.LineMessage(line), getSelf()));
		} catch (IOException e) {
			logger.error("Error while sending 'LineMessage' message to AggregatorActor", e);

			// Disposing ActorSystem

			logger.info("Terminating ActorSystem '{}'", ACTOR_SYSTEM_NAME);
			Util.terminateActorSystem(getContext());
		}

		// Sending 'End of file' message(s) to aggregator actor in order to
		// inform the file reading process has completed and to display the accumulated word count 

		logger.info("Sending '{}' message to actor '{}'", "EndOfFileMessage", AGGREGATOR_ACTOR_NAME);
		aggregatorActorRef.tell(new AggregatorActor.EndOfFileMessage(), getSelf());
	}
}
