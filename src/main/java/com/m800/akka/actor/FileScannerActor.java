package com.m800.akka.actor;

import static com.m800.akka.common.Constant.ACTOR_SYSTEM_NAME;
import static com.m800.akka.common.Constant.FILE_PARSER_ACTOR_NAME;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.m800.akka.common.Util;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;

/**
 * File Scanner Actor to verify given file existense and directing File Parser
 * Actor to parse lines in the file
 *
 */
public class FileScannerActor extends AbstractActor {
	static final Logger logger = LoggerFactory.getLogger(FileScannerActor.class);
	
	/**
	 * Initializing relevant actor properties
	 */
	public static Props props(String filePath, ActorRef fileParserActor) {
		return Props.create(FileScannerActor.class, () -> new FileScannerActor(filePath, fileParserActor));
	}

	/**
	 * Definition of 'Scan' message for scanning a file
	 * 
	 */
	public static class ScanMessage {
	}

	private final String filePath;
	private final ActorRef fileParserActorRef;
	
	public FileScannerActor(String filePath, ActorRef fileParserActorRef) {
		this.filePath = filePath;
		this.fileParserActorRef = fileParserActorRef;
	}

	/**
	 * Callback method triggered prior to receiving a message 
	 */
	@Override
	public void preStart() {
		logger.debug("Starting '{}'", getSelf().path().name());
	}

	/**
	 * Callback method triggered prior to restarting an actor
	 */
	@Override
	public void preRestart(Throwable reason, Optional<Object> message) {
		logger.error("Restarting due to {} when processing {}", reason.getMessage(), message.isPresent() ? message.get() : "");
	}

	/**
	 * Callback method triggered on receiving a message
	 */
	@Override
	public Receive createReceive() {
		return receiveBuilder().match(ScanMessage.class, this::onScan).build();
	}

	/**
	 * Message handler to check existence of a given file and send parsing message to file parser actor
	 */
	private void onScan(ScanMessage scanMessage) {
		logger.debug("Scanning file '{}'", filePath);
		
		// Checking if a given file exists
		
		if (Util.isFileExist(this.filePath)) {
			// Sending 'Parse' message to file parser actor
			
			logger.info("Sending '{}' message to actor '{}'", "ParseMessage", FILE_PARSER_ACTOR_NAME);
			fileParserActorRef.tell(new FileParserActor.ParseMessage(filePath), getSelf());
		} else {
			// Reporting an error if the given file does not exist

			logger.error("No such file exists '{}'", filePath);
			
			// Disposing ActorSystem
			
			logger.info("Terminating ActorSystem '{}'", ACTOR_SYSTEM_NAME);
			Util.terminateActorSystem(getContext());
		}
	}
}
