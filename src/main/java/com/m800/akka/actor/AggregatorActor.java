package com.m800.akka.actor;

import static com.m800.akka.common.Constant.ACTOR_SYSTEM_NAME;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.m800.akka.common.Util;

import akka.actor.AbstractActor;
import akka.actor.Props;

/**
 * Aggregator Actor for counting and accumulating words
 *
 */
public class AggregatorActor extends AbstractActor {
	static final Logger logger = LoggerFactory.getLogger(AggregatorActor.class);
	
	/**
	 * Initializing relevant actor properties
	 */
	public static Props props() {
		return Props.create(AggregatorActor.class);
	}
	
	/**
	 * Definition of 'Start of file' message to prepare for counting the words in a given line
	 * 
	 */
	public static class StartOfFileMessage {
	}

	/**
	 * Definition of 'Line' message to count and accumulate the words in a given line
	 * 
	 */
	public static class LineMessage {
		private final String line;
		
		public LineMessage(String line) {
			this.line = line;
		}

		@Override
		public String toString() {
			return "LineMessage [line=" + line + "]";
		}
	}

	/**
	 * Definition of 'End of file' message to display the accumulated word count
	 * 
	 */
	public static class EndOfFileMessage {
	}

	private int wordCount;

	/**
	 * Callback method triggered prior to receiving a message 
	 */
	@Override
	public void preStart() {
		logger.debug("Starting '{}'", getSelf().path().name());
	}

	/**
	 * Callback method triggered prior to restarting an actor
	 */
	@Override
	public void preRestart(Throwable reason, Optional<Object> message) {
		logger.error("Restarting due to {} when processing {}", reason.getMessage(), message.isPresent() ? message.get() : "");
	}

	/**
	 * Callback method triggered on receiving a message
	 */
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(StartOfFileMessage.class, this::onStartOfFile) // Matching 'Start of file' message
				.match(LineMessage.class, this::onLine) // Matching 'Line' message
				.match(EndOfFileMessage.class, this::onEndOfFile) // Matching 'End of file' message
				.build();
	}

	/**
	 * Message handler to prepare for counting the words in a given line
	 */
	private void onStartOfFile(StartOfFileMessage startOfFileMessage) {
		logger.debug("Starting to read file");
		this.wordCount = 0;
	}

	/**
	 * Message handler to count and accumulate the words in a given line
	 */
	private void onLine(LineMessage lineMessage) {
		String line = lineMessage.line;

		logger.trace("Reading line '{}'", line);
		
		if (!line.equals("")) {
			int lineWordCount = line.split("\\s+").length;
			this.wordCount += lineWordCount;
		} else {
			logger.trace("Ignoring empty line");
		}
	}

	/**
	 * Message handler to display the accumulated word count
	 */
	private void onEndOfFile(EndOfFileMessage endOfFileMessage) {
		logger.debug("End of file reached");
		logger.info("Total word count is {}", this.wordCount);
	
		logger.info("Terminating ActorSystem '{}'", ACTOR_SYSTEM_NAME);
		Util.terminateActorSystem(getContext());
	}
}
