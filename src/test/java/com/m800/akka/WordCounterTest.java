package com.m800.akka;

import static com.m800.akka.common.Constant.FILE_SCANNER_ACTOR_NAME;
import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.m800.akka.actor.FileParserActor;
import com.m800.akka.actor.FileScannerActor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;

public class WordCounterTest {
	static ActorSystem system;
	static File file;
	
	@BeforeClass
    public static void setup() {
    	String filePath = "src/test/resources/file/sample.log";
    	file = new File(filePath);
		
		system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }
    
    @Test
    public void testFileScanner() {
        final TestKit testProbe = new TestKit(system);
        
        final ActorRef fileScannerActorRef = system.actorOf(FileScannerActor.props(file.getAbsolutePath(), testProbe.getRef()), FILE_SCANNER_ACTOR_NAME);

        fileScannerActorRef.tell(new FileScannerActor.ScanMessage(), ActorRef.noSender());
        
        FileParserActor.ParseMessage parserMessage = testProbe.expectMsgClass(FileParserActor.ParseMessage.class);
        
        assertEquals(file.getAbsolutePath(), parserMessage.filePath);
    }
}